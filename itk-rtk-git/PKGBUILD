# Maintainer: Andreas Gravgaard Andersen <andreasga22 at gmail dot com>

_pkgname=itk
pkgname=$_pkgname-rtk-git
pkgver=5.1.0.230.g85cb1d5860
pkgrel=1
pkgdesc='Open-source, cross-platform C++ toolkit for segmentation and registration'
arch=('i686' 'x86_64')
url='http://www.itk.org'
license=('Apache')
depends=('hdf5-cpp-fortran' 'gdcm-git' 'libjpeg-turbo' 'libpng' 'libtiff')
makedepends=('git' 'cmake' 'intel-mkl-static')
provides=("$_pkgname" "insight-toolkit"{,-git} "rtk"{,-git} "lpsolve")
conflicts=("$_pkgname" "insight-toolkit"{,-git} "rtk"{,-git} "lpsolve")
source=("$_pkgname::git://git.code.sf.net/p/itk/code")
sha256sums=("SKIP")

pkgver() {
  cd $_pkgname
  git describe --always | sed 's:^v::;s:-:.:g'
}

build() {
  cd $srcdir

  rm -Rf build && mkdir build
  cd build
  cmake $srcdir/$_pkgname \
    -DBUILD_SHARED_LIBS=ON \
    -DBUILD_TESTING=OFF \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DITK_BUILD_DEFAULT_MODULES=OFF \
    -DITK_COMPUTER_MEMORY_SIZE=128 \
    -DModule_RTK=ON \
    -DModule_ITKReview=ON \
    -DModule_ITKDCMTK=ON \
    -DModule_ITKTBB=ON \
    -DModule_ITKGDCM=ON \
    -DModule_ITKIOGDCM=ON \
    -DModule_ITKCudaCommon=OFF \
    -DModule_ITKDeprecated=ON \
    -DITK_USE_MKL=ON \
    -DITK_USE_FFTWD=ON \
    -DITK_USE_FFTWF=ON \
    -DITK_USE_GPU=ON \
    -DRTK_USE_CUDA=OFF \
    -DITK_USE_SYSTEM_LIBRARIES=ON \
    -DITK_USE_SYSTEM_GDCM=ON \
    -DITK_USE_SYSTEM_DCMTK=ON \
    -DITK_USE_SYSTEM_FFTW=ON \
    -DITK_WRAP_JAVA=OFF \
    -DITK_WRAP_PERL=OFF \
    -DITK_WRAP_PYTHON=OFF \
    -DITK_WRAP_RUBY=OFF \
    -DITK_WRAP_TCL=OFF

  make
}

package() {
  cd build
  make DESTDIR="$pkgdir" install
}
